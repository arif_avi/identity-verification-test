How To Execute
========================

1. git clone https://gitlab.com/arif_avi/identity-verification-test.git
2. cd identity-verification-test
3. composer install
4. The CSV file should be placed inside the /web folder. The below command takes filename as parameter. 
5. php bin/console identification-requests:process input.csv