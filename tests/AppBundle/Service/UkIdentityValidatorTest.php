<?php

// tests/AppBundle/Service/UkIdentityValidatorTest.php

namespace Tests\AppBundle\Service;

use AppBundle\Service\UkIdentityValidator;
use PHPUnit\Framework\TestCase;

class UkIdentityValidatorTest extends TestCase
{
    protected function getObjectArray($data)
    {
        $columns = [
            'requestDate',
            'countryCode',
            'documentType',
            'documentNumber',
            'issueDate',
            'personalIdentificationNumber',
        ];

        $item = [];
        foreach ($columns as $k => $v):
            $item[$v] = $data[$k];
        endforeach;

        return $item;
    }

    public function testIdentityObject()
    {
        $data = [
            ['2019-01-04', 'uk', 'identity_card', '64053869', '2015-09-07', '840252118'],
            ['2019-01-04', 'uk', 'identity_card', '64053869', '2019-09-07', '840252118'],
        ];

        $testCommand = new \AppBundle\Command\TestCommand();
        $identityValidatory = new UkIdentityValidator();

        $pidArray = $testCommand->getPidArray($data);

        $objectData = $this->getObjectArray($data[0]);
        $identityValidatory->setData($objectData, $pidArray);
        $result = $identityValidatory->checkDocumentType();
        $this->assertTrue($result['success']);

        $objectData = $this->getObjectArray($data[1]);
        $identityValidatory->setData($objectData, $pidArray);
        $result = $identityValidatory->checkDocumentType();

        //after 2019 only passports are applicable
        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('msg', $result);
        $this->assertFalse($result['success']);
        $this->assertSame('document_type_is_invalid', $result['msg']);

        $result = $identityValidatory->checkDocumentLength();
        $this->assertTrue($result['success']);

        //set invalid document number
        $identityValidatory->documentNumber = '1234567890000';
        $result = $identityValidatory->checkDocumentLength();
        $this->assertFalse($result['success']);
        $this->assertNotEquals('document_type_is_invalid', $result['msg']);
        $this->assertSame('document_number_length_invalid', $result['msg']);
    }
}
