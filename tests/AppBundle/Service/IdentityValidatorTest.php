<?php

// tests/AppBundle/Service/IdentityValidatorTest.php

namespace Tests\AppBundle\Service;

use AppBundle\Service\IdentityValidator;
use PHPUnit\Framework\TestCase;

class IdentityValidatorTest extends TestCase
{
    protected function getObjectArray($data)
    {
        $columns = [
            'requestDate',
            'countryCode',
            'documentType',
            'documentNumber',
            'issueDate',
            'personalIdentificationNumber',
        ];

        $item = [];
        foreach ($columns as $k => $v):
            $item[$v] = $data[$k];
        endforeach;

        return $item;
    }

    public function testIdentityObject()
    {
        $data = [
            ['2019-01-01', 'lt', 'passport', '30122719', '2019-03-01', '357717289'],
            ['2019-01-03', 'de', 'indentity_card', '142532922222', '2009-01-01', '962044284'],
        ];

        $testCommand = new \AppBundle\Command\TestCommand();
        $identityValidatory = new IdentityValidator();

        $pidArray = $testCommand->getPidArray($data);

        $objectData = $this->getObjectArray($data[0]);
        $identityValidatory->setData($objectData, $pidArray);
        $result = $identityValidatory->checkDocumentType();
        $this->assertTrue($result['success']);

        $objectData = $this->getObjectArray($data[1]);
        $identityValidatory->setData($objectData, $pidArray);
        $result = $identityValidatory->checkDocumentType();

        $this->assertArrayHasKey('success', $result);
        $this->assertArrayHasKey('msg', $result);

        $this->assertFalse($result['success']);
        $this->assertSame('document_type_is_invalid', $result['msg']);

        $result = $identityValidatory->checkDocumentLength();
        $this->assertFalse($result['success']);
        $this->assertNotEquals('document_length_is_asSs', $result['msg']);
        $this->assertNotEquals('document_length_is_invalid', $result['msg']);
    }
}
