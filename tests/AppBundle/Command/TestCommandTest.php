<?php

// tests/AppBundle/Command/TestCommandTest.php

namespace Tests\AppBundle\Command;

use AppBundle\Command\TestCommand;
use PHPUnit\Framework\TestCase;

class TestCommandTest extends TestCase
{
    public function testValidateIdentity()
    {
        $testCommand = new TestCommand();

        $data = [
            ['2019-01-01', 'lt', 'passport', '30122719', '2019-03-01', '357717289'],
            ['2019-01-03', 'de', 'indentity_card', '14253292', '2009-01-01', '962044284'],
        ];

        $pidArray = $testCommand->getPidArray($data);
        $identityExtractor = new \AppBundle\Service\IdentityExtractor($data[0], new \AppBundle\Service\IdentityValidator(), $pidArray);

        $identity = $identityExtractor->getIdentityObject();

        $result = $testCommand->validateIdentity($identity);
        //checking lituanian data
        $this->assertEquals('valid', $result['msg']);

        $identityExtractor = new \AppBundle\Service\IdentityExtractor($data[0], new \AppBundle\Service\DeIdentityValidator(), $pidArray);
        $identity = $identityExtractor->getIdentityObject();
        
        $result = $testCommand->validateIdentity($identity);
        //checking german data
        $this->assertEquals('valid', $result['msg']);

        //check lithuanian data but change to identity card
        $data[0][2] = 'identity_card';
        $identityExtractor = new \AppBundle\Service\IdentityExtractor($data[0], new \AppBundle\Service\DeIdentityValidator(), $pidArray);
        $identity = $identityExtractor->getIdentityObject();

        $result = $testCommand->validateIdentity($identity);
        //checking german data with identity card but same personal id
        $this->assertEquals('request_limit_exceeded', $result['msg']);

        //check german data but change to identity card
        $identityExtractor = new \AppBundle\Service\IdentityExtractor($data[1], new \AppBundle\Service\DeIdentityValidator(), $pidArray);
        $identity = $identityExtractor->getIdentityObject();

        $result = $testCommand->validateIdentity($identity);
        //checking german data with identity card should return invalid document type
        $this->assertEquals('document_type_is_invalid', $result['msg']);
    }

    public function testValidateIdentityDocumentCheck()
    {
        $testCommand = new TestCommand();

        $data = [
            ['2019-01-01', 'lt', 'passport', '3012271966', '2019-03-01', '357717289'],
            ['2019-01-03', 'de', 'passport', '14253292', '2009-01-01', '962044284'],
            ['2019-01-03', 'de', 'identity_card', '14253292', '2010-01-01', '962044284'],
        ];

        $pidArray = $testCommand->getPidArray($data);

        $identityExtractor = new \AppBundle\Service\IdentityExtractor($data[0], new \AppBundle\Service\IdentityValidator(), $pidArray);
        $identity = $identityExtractor->getIdentityObject();

        $result = $testCommand->validateIdentity($identity);
        //checking lituanian data
        $this->assertEquals('document_number_length_invalid', $result['msg']);

        $identityExtractor = new \AppBundle\Service\IdentityExtractor($data[1], new \AppBundle\Service\DeIdentityValidator(), $pidArray);
        $identity = $identityExtractor->getIdentityObject();

        $result = $testCommand->validateIdentity($identity);
        //checking german data
        $this->assertEquals('document_is_expired', $result['msg']);

        $identityExtractor = new \AppBundle\Service\IdentityExtractor($data[2], new \AppBundle\Service\DeIdentityValidator(), $pidArray);
        $identity = $identityExtractor->getIdentityObject();

        $result = $testCommand->validateIdentity($identity);
        //checking german data with identity card but same personal id
        $this->assertEquals('valid', $result['msg']);
    }
}
