<?php

namespace AppBundle\Command;

use AppBundle\Service\IdentityExtractor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends Command
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('identification-requests:process')
            // the short description shown while running "php bin/console list"
            ->setDescription('Identification Process Function')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command will take filename as the argument. File should reside on the web directory')
            //take the file name
            ->addArgument('filename', InputArgument::REQUIRED, 'filename is needed. file should be placed in the web directory')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //$output->write("Start of process identification");

        // Get providen text using the $input->getArgument method.
        $filename = $input->getArgument('filename');

        //$output->writeln("Provided Filename : ".$filename);

        $reader = \League\Csv\Reader::createFromPath('./web/'.$filename);

        $result = $reader->fetchAll();
        //print_r($result);

        $pidArray = $this->getPidArray($result);
        //print_r($pidArray);
        foreach ($result as $r) {
            $identity = null;
            switch ($r[1]) {
                case 'lt':
                    $identity = new IdentityExtractor($r, new \AppBundle\Service\IdentityValidator(), $pidArray);

                    break;

                case 'de':
                    $identity = new IdentityExtractor($r, new \AppBundle\Service\DeIdentityValidator(), $pidArray);

                    break;

                case 'es':
                    $identity = new IdentityExtractor($r, new \AppBundle\Service\EsIdentityValidator(), $pidArray);

                    break;

                case 'fr':
                    $identity = new IdentityExtractor($r, new \AppBundle\Service\FrIdentityValidator(), $pidArray);

                    break;

                case 'pl':
                    $identity = new IdentityExtractor($r, new \AppBundle\Service\PlIdentityValidator(), $pidArray);

                    break;

                case 'it':
                    $identity = new IdentityExtractor($r, new \AppBundle\Service\ItIdentityValidator(), $pidArray);

                    break;

                case 'uk':
                    $identity = new IdentityExtractor($r, new \AppBundle\Service\UkIdentityValidator(), $pidArray);

                    break;
                default:
                    continue;
                    break;
            }

            if (null == $identity):
                continue;
            endif;

            $identityObject = $identity->getIdentityObject();
            $result = $this->validateIdentity($identityObject);
            $output->writeln($result['msg']);
        }

        // outputs a message without adding a "\n" at the end of the line
        //$output->write("End of process identification");
    }

    /**
     * Get the data in personal identification number as key and value should contain the week number.
     *
     * @param array $csvData
     */
    public function getPidArray($csvData)
    {
        $items = [];
        foreach ($csvData as $k => $v):
            $requestDate = date_create_from_format('Y-m-d', $v[0]);
            if (!isset($items[$v[5]])):
                $items[$v[5]] = [
                    'data' => [],
                    'week' => [],
                ];
            endif;

            //$items[$v[5]]['data'][] = $v;

            if (!isset($items[$v[5]]['week'][$requestDate->format('W')])):
                    $items[$v[5]]['week'][$requestDate->format('W')] = 0;
            endif;
            //$items[$v[5]]['week'][$requestDate->format("W")]++;
        endforeach;

        return $items;
    }

    public function validateIdentity($identity)
    {
        $documentTypeCheck = $identity->checkDocumentType();
        if (!$documentTypeCheck['success']) {
            return $documentTypeCheck;
        }

        $documentLengthCheck = $identity->checkDocumentLength();
        if (!$documentLengthCheck['success']) {
            return $documentLengthCheck;
        }

        $documentNumberCheck = $identity->checkDocumentNumber();
        if (!$documentNumberCheck['success']) {
            return $documentNumberCheck;
        }

        $documentIssueDateCheck = $identity->checkDocumentIssueDate();
        if (!$documentIssueDateCheck['success']) {
            return $documentIssueDateCheck;
        }

        $documentValidityCheck = $identity->checkDocumentValidity();
        if (!$documentValidityCheck['success']) {
            return $documentValidityCheck;
        }

        $clientRequestCountCheck = $identity->checkClientRequestCount();
        if (!$clientRequestCountCheck['success']) {
            return $clientRequestCountCheck;
        }

        return [
           'success' => true,
           'msg' => 'valid',
       ];
    }
}
