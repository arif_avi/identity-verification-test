<?php

// src/AppBundle/Service/EsIndentityValidator.php

namespace AppBundle\Service;

class EsIdentityValidator extends IdentityValidator
{
    public $newDocumentValidityThreshHold = '2000-02-14'; //'2013-02-14';
    public $stolenPassportSerialUpperRange = [50001111, 50009999];

    public function __construct()
    {
        parent::__construct();
        $this->documentTypes['passport']['newDocumentValidityTime'] = 15;
    }

    public function checkDocumentNumber()
    {
        if ('passport' == $this->documentType && ($this->documentNumber >= $this->stolenPassportSerialUpperRange[0] && $this->documentNumber <= $this->stolenPassportSerialUpperRange[1])) {
            return [
                'success' => false,
                'msg' => self::INVALID_DOCUMENT_NUMBER,
                'log' => 'DocumentNumber: '.$this->documentNumber.' is in side the stolen range',
                'range' => json_encode($this->stolenPassportSerialUpperRange),
            ];
        }

        return [
            'success' => true,
        ];
    }

    public function checkDocumentValidity()
    {
        try {
            $issueDate = date_create_from_format('Y-m-d', $this->issueDate);
            $currentDate = date_create_from_format('Y-m-d', date('Y-m-d'));

            $diff = $currentDate->diff($issueDate);

            $validityTime = $this->documentTypes[$this->documentType]['documentValidityTime'];

            $thresholdDate = date_create_from_format('Y-m-d', $this->newDocumentValidityThreshHold);
            if ($issueDate >= $thresholdDate && 'passport' == $this->documentType) {
                $validityTime = $this->documentTypes[$this->documentType]['newDocumentValidityTime'];
            }

            if ($diff->y > $validityTime) {
                return [
                    'success' => false,
                    'msg' => self::DOCUMENT_EXPIRED,
                    'log' => $this->documentTypes[$this->documentType],
                    'log2' => 'Issue date is '.$diff->y.' years old',
                ];
            }

            return [
                'success' => true,
            ];
        } catch (Exception $ex) {
            return [
                'success' => false,
                'msg' => self::DOCUMENT_EXPIRED,
                'log' => $ex->getMessage(),
            ];
        }
    }
}
