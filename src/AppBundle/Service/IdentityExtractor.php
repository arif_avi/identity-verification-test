<?php

// src/AppBundle/Service/IdentityExtractor.php

namespace AppBundle\Service;

class IdentityExtractor
{
    public $classPrefix = '';
    public $className = 'IdentityValidator';
    public $dataColumns = [
        'requestDate',
        'countryCode',
        'documentType',
        'documentNumber',
        'issueDate',
        'personalIdentificationNumber',
    ];
    public $data = [];
    public $object = null;

    public function __construct($data, $iv, &$pidArray)
    {
        foreach ($this->dataColumns as $dk => $dv) {
            $this->data[$dv] = $data[$dk];
        }
        $iv->setData($this->data, $pidArray);
        $this->object = $iv;
        //print_r($iv->identityObject);
        //$iv->validateIdentity();
    }

    public function getIdentityObject()
    {
        return $this->object;
    }
}
