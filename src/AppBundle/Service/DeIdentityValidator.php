<?php

// src/AppBundle/Service/DeIndentityValidator.php

namespace AppBundle\Service;

class DeIdentityValidator extends IdentityValidator
{
    public $newDocumentValidityThreshHold = '2010-01-01';

    public function __construct()
    {
        parent::__construct();
        $this->documentTypes['identity_card']['newDocumentValidityTime'] = 10;
    }

    public function checkDocumentValidity()
    {
        try {
            $issueDate = date_create_from_format('Y-m-d', $this->issueDate);
            $currentDate = date_create_from_format('Y-m-d', date('Y-m-d'));

            $diff = $currentDate->diff($issueDate);

            $validityTime = $this->documentTypes[$this->documentType]['documentValidityTime'];

            $thresholdDate = date_create_from_format('Y-m-d', $this->newDocumentValidityThreshHold);
            if ($issueDate >= $thresholdDate && 'identity_card' == $this->documentType) {
                $validityTime = $this->documentTypes[$this->documentType]['newDocumentValidityTime'];
            }

            if ($diff->y > $validityTime) {
                return [
                    'success' => false,
                    'msg' => self::DOCUMENT_EXPIRED,
                    'log' => $this->documentTypes[$this->documentType],
                    'fn' => __FUNCTION__,
                    'cl' => __FILE__,
                ];
            }

            return [
                'success' => true,
            ];
        } catch (Exception $ex) {
            return [
                'success' => false,
                'msg' => self::DOCUMENT_EXPIRED,
                'log' => $ex->getMessage(),
                'fn' => __FUNCTION__,
                'cl' => __FILE__,
            ];
        }
    }
}
