<?php

// src/AppBundle/Service/ItIndentityValidator.php

namespace AppBundle\Service;

class ItIdentityValidator extends IdentityValidator
{
    public $newDocumentCheckingDayThresholdStart = '2019-01-01';
    public $newDocumentCheckingDayThresholdEnd = '2019-01-31';
    public $newDocumentCheckingDay = ['Saturday'];

    public function __construct()
    {
        parent::__construct();
    }

    public function checkDocumentIssueDate()
    {
        try {
            $overtimeStart = date_create_from_format('Y-m-d', $this->newDocumentCheckingDayThresholdStart);
            $overtimeEnd = date_create_from_format('Y-m-d', $this->newDocumentCheckingDayThresholdEnd);

            $issueDate = date_create_from_format('Y-m-d', $this->issueDate);

            $invalidIssueDays = $this->invalidIssueDays;
            if ($issueDate >= $overtimeStart && $issueDate <= $overtimeEnd) {
                $invalidIssueDays = array_diff($invalidIssueDays, $this->newDocumentCheckingDay);
            }

            if (in_array($issueDate->format('l'), $invalidIssueDays)) {
                return [
                    'success' => false,
                    'msg' => self::INVALID_ISSUE_DATE,
                    'log' => 'IssuedOn : '.$issueDate->format('l'),
                    'log2' => $issueDate->format('Y-m-d'),
                ];
            }

            return [
                'success' => true,
            ];
        } catch (Exception $ex) {
            return [
                'success' => false,
                'msg' => self::INVALID_ISSUE_DATE,
                'log' => $ex->getMessage(),
            ];
        }
    }
}
