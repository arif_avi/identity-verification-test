<?php

// src/AppBundle/Service/IndentityValidator.php

namespace AppBundle\Service;

class IdentityValidator
{
    public $documentTypes = [
        'passport' => [
            'documentValidityTime' => 5,
        ],
        'identity_card' => [
            'documentValidityTime' => 5,
        ],
        'residence_permit' => [
            'documentValidityTime' => 5,
        ],
    ];

    //in years
    public $documentValidityTime = 5;
    public $documentNumberLength = 8;
    public $invalidIssueDays = ['Saturday', 'Sunday'];
    public $maxValidationAttempts = 2;
    public $data = [];
    public $identityObject = null;
    public $pidArray = [];

    public $requestDate = null;
    public $documentType = null;
    public $documentNumber = null;
    public $issueDate = null;
    public $personalIdentificationNumber = null;

    const INVALID_LENGTH = 'document_number_length_invalid';
    const INVALID_TYPE = 'document_type_is_invalid';
    const INVALID_ISSUE_DATE = 'document_issue_date_invalid';
    const INVALID_DOCUMENT_NUMBER = 'document_number_invalid';
    const DOCUMENT_EXPIRED = 'document_is_expired';
    const REQUEST_LIMIT_EXCEEDED = 'request_limit_exceeded';

    public function __construct()
    {
    }

    public function setData($data, &$pidArray)
    {
        $this->requestDate = $data['requestDate'];
        $this->documentType = $data['documentType'];
        $this->documentNumber = $data['documentNumber'];
        $this->issueDate = $data['issueDate'];
        $this->personalIdentificationNumber = $data['personalIdentificationNumber'];
        $this->pidArray = &$pidArray;
    }

    public function checkDocumentType()
    {
        if (!in_array($this->documentType, array_keys($this->documentTypes))) {
            return [
                'success' => false,
                'msg' => self::INVALID_TYPE,
                'fn' => __FUNCTION__,
                'cl' => __CLASS__,
                'dt' => $this->documentType,
                'it' => json_encode($this->documentTypes),
            ];
        }

        return [
            'success' => true,
        ];
    }

    public function checkDocumentLength()
    {
        if (mb_strlen($this->documentNumber) != $this->documentNumberLength) {
            return [
                'success' => false,
                'msg' => self::INVALID_LENGTH,
                'fn' => __FUNCTION__,
                'cl' => __CLASS__,
            ];
        }

        return [
            'success' => true,
        ];
    }

    public function checkDocumentNumber()
    {
        return [
            'success' => true,
            'fn' => __FUNCTION__,
            'cl' => __CLASS__,
        ];
    }

    public function checkDocumentIssueDate()
    {
        try {
            $issueDate = date_create_from_format('Y-m-d', $this->issueDate);

            if (in_array($issueDate->format('l'), $this->invalidIssueDays)) {
                return [
                    'success' => false,
                    'msg' => self::INVALID_ISSUE_DATE,
                    'fn' => __FUNCTION__,
                    'cl' => __CLASS__,
                ];
            }

            return [
                'success' => true,
            ];
        } catch (Exception $ex) {
            return [
                'success' => false,
                'msg' => self::INVALID_ISSUE_DATE,
                'log' => $ex->getMessage(),
                'fn' => __FUNCTION__,
                'cl' => __CLASS__,
            ];
        }
    }

    public function checkDocumentValidity()
    {
        try {
            $issueDate = date_create_from_format('Y-m-d', $this->issueDate);
            $currentDate = date_create_from_format('Y-m-d', date('Y-m-d'));

            $diff = $currentDate->diff($issueDate);

            if ($diff->y > $this->documentTypes[$this->documentType]['documentValidityTime']) {
                return [
                    'success' => false,
                    'msg' => self::DOCUMENT_EXPIRED,
                    'log' => $this->documentTypes[$this->documentType],
                    'fn' => __FUNCTION__,
                    'cl' => __CLASS__,
                ];
            }

            return [
                'success' => true,
            ];
        } catch (Exception $ex) {
            return [
                'success' => false,
                'msg' => self::DOCUMENT_EXPIRED,
                'log' => $ex->getMessage(),
                'fn' => __FUNCTION__,
                'cl' => __CLASS__,
            ];
        }
    }

    public function checkClientRequestCount()
    {
        try {
            $requestDate = date_create_from_format('Y-m-d', $this->requestDate);
            ++$this->pidArray[$this->personalIdentificationNumber]['week'][$requestDate->format('W')];
            if ($this->pidArray[$this->personalIdentificationNumber]['week'][$requestDate->format('W')] > $this->maxValidationAttempts):
                return [
                    'success' => false,
                    'msg' => self::REQUEST_LIMIT_EXCEEDED,
                ];
            endif;
        } catch (Exception $ex) {
            return [
                'success' => false,
                'msg' => self::REQUEST_LIMIT_EXCEEDED,
            ];
        }

        return [
            'success' => true,
        ];
    }
}
