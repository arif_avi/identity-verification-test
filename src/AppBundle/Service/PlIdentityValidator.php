<?php

// src/AppBundle/Service/PlIndentityValidator.php

namespace AppBundle\Service;

class PlIdentityValidator extends IdentityValidator
{
    public $newDocumentNumberThreshHold = '2018-09-01';
    public $newDocumentNumberLength = 10;

    public function __construct()
    {
        parent::__construct();
    }

    public function checkDocumentLength()
    {
        try {
            $issueDate = date_create_from_format('Y-m-d', $this->issueDate);
            $newDocumentNumberThreshHold = date_create_from_format('Y-m-d', $this->newDocumentNumberThreshHold);
            $documentLength = $this->documentNumberLength;
            if ('identity_card' == $this->documentType && $issueDate >= $newDocumentNumberThreshHold):
                $documentLength = $this->newDocumentNumberLength;
            endif;

            if (mb_strlen($this->documentNumber) != $documentLength) {
                return [
                    'success' => false,
                    'msg' => self::INVALID_LENGTH,
                    'log' => $this->documentNumber.' has length '.mb_strlen($this->documentNumber),
                ];
            }

            return [
                'success' => true,
            ];
        } catch (Exception $ex) {
            return [
                'success' => false,
                'msg' => self::INVALID_LENGTH,
            ];
        }
    }
}
