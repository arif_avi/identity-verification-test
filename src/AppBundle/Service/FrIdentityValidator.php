<?php

// src/AppBundle/Service/FrIndentityValidator.php

namespace AppBundle\Service;

class FrIdentityValidator extends IdentityValidator
{
    public function __construct()
    {
        parent::__construct();
        $this->documentTypes['drivers_license'] = [
            'documentValidityTime' => 5,
        ];
    }
}
