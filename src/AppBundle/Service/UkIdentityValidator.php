<?php

// src/AppBundle/Service/UkIndentityValidator.php

namespace AppBundle\Service;

class UkIdentityValidator extends IdentityValidator
{
    public $newDocumentTypeThreshold = '2019-01-01';

    public function __construct()
    {
        parent::__construct();
    }

    public function checkDocumentType()
    {
        try {
            $newDocumentTypeThreshold = date_create_from_format('Y-m-d', $this->newDocumentTypeThreshold);
            $issueDate = date_create_from_format('Y-m-d', $this->issueDate);

            $documentTypes = $this->documentTypes;
            if ($issueDate >= $newDocumentTypeThreshold) {
                $documentTypes = [];
                $documentTypes['passport'] = $this->documentTypes['passport'];
            }

            if (!in_array($this->documentType, array_keys($documentTypes))) {
                return [
                    'success' => false,
                    'msg' => self::INVALID_TYPE,
                    'fn' => __FUNCTION__,
                    'cl' => __CLASS__,
                ];
            }

            return [
                'success' => true,
            ];
        } catch (Exception $ex) {
            return [
                'success' => false,
                'msg' => self::INVALID_TYPE,
                'log' => $ex->getMessage(),
            ];
        }
    }
}
